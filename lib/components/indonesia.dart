import 'package:flutter/material.dart';
import '../widgets/summary_card.dart';

class Indonesia extends StatelessWidget {
  final double height;
  final data;

  Indonesia(
      {this.height,
      this.data}); //TERIMA DATA YANG DIKIRIMKAN KETIKA COMPONENT INDONESIA DIGUNAKAN OLEH HOME.DART

  @override
  Widget build(BuildContext context) {
    //PISAHKAN LAGI MENJADI DUA BUAH KOLOM
    return Column(
      children: <Widget>[
        //BAGIAN INI ADALAH MASING-MASING DATANYA
        Expanded(
          flex: 2,
          //YANG AKAN DITAMPILKAN MENGGUNAKAN GRIDVIEW
          child: GridView.count(
            childAspectRatio: height / 200, //DENGAN RASIO TINGGI LAYAR BAGI 350
            crossAxisCount: 1, //HANYA ADA DUA WIDGET DALAM SATU DERETAN
            crossAxisSpacing: 10, //INI UNTUK JARAK MASING-MASING WIDGET
            mainAxisSpacing: 10,
            children: <Widget>[
              //ADAPUN CARD YANG AKAN MERENDER DATANYA KITA PISAHKAN LAGI KE COMPONENT SENDIRI
              //DIMANA DATA YANG DIKIRIMKAN ADALAH TOTAL, LABEL, WARNA DAN UKURAN HURUFNYA
              SummaryCard(
                total: data.summary.confirmed.toString(),
                label: 'Terkonfirmasi',
                color: Colors.yellow[800],
                size: 35,
                icon: Icons.report,
              ),
              SummaryCard(
                total: data.summary.activeCare.toString(),
                label: 'Dalam Perawatan',
                color: Colors.purple[500],
                size: 35,
                icon: Icons.accessible,
              ),
              SummaryCard(
                total: data.summary.recovered.toString(),
                label: 'Sembuh',
                color: Colors.teal[500],
                size: 35,
                icon: Icons.healing,
              ),
              SummaryCard(
                total: data.summary.deaths.toString(),
                label: 'Meninggal',
                color: Colors.red[800],
                size: 35,
                icon: Icons.block,
              ),
            ],
          ),
        ),
        Text('Pembaruan Terakhir'),
        Text(data.updated),
      ],
    );
  }
}
