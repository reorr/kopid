import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import './providers/corona_provider.dart';
import './home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => CoronaProvider(),
        )
      ],
      child: MaterialApp(
          theme: ThemeData(
            // brightness: Brightness.light,
            brightness: Brightness.dark,
            primaryColor: Colors.red,
          ),
          darkTheme: ThemeData(
            brightness: Brightness.dark,
          ),
          debugShowCheckedModeBanner: false,
          home: Home(),
          title: 'Kopid'),
    );
  }
}
