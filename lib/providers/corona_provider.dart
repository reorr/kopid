import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './indonesia_model.dart';

class CoronaProvider with ChangeNotifier {
  IndonesiaModel summary; //STATE UNTUK MENAMPUNG DATA DI INDONESIA
  String updated; //STATE UNTUK MENAMPUNG WAKTU PEMBAHARUAN

  //FUNGSI INI AKAN MENJALANKAN API CALL UNTUK MENGAMBIL DATA
  Future<void> getData() async {
    //BAGIAN PERTAMA ADALAH UNTUK MENGAMBIL DATA DARI INDONESIA
    final url = 'https://kawalcovid19.harippe.id/api/summary';
    final response = await http.get(url); //HIT KE API
    //DAN CONVERT DATA YANG DITERIMA
    final result = json.decode(response.body) as Map<String, dynamic>;
    //LALU MASUKKAN KE DALAM STATE SUMMARY DENGAN FORMAT BERDASARKAN INDONESIAMODEL
    summary = IndonesiaModel(
      confirmed: result['confirmed']['value'],
      recovered: result['recovered']['value'],
      deaths: result['deaths']['value'],
      activeCare: result['activeCare']['value'],
    );
    //SIMPAN DATA PEMBAHARUAN KE DALAM STATE UPDATED
    updated = result['metadata']['lastUpdatedAt'];
    notifyListeners(); //INFORMASIKAN BAHWA TERJADI PERUBAHAN STATE AGAR WIDGET DIRENDER ULANG
  }
}
