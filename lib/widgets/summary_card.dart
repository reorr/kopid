import 'package:flutter/material.dart';

class SummaryCard extends StatelessWidget {
  final String total;
  final String label;
  final Color color;
  final int size;
  final IconData icon;

  final middleSection = Expanded(
      child: Container(
          child: Column(
    children: <Widget>[],
  )));
  //HANDLE DATA YANG DIKIRIMKAN
  SummaryCard({
    @required this.total,
    @required this.label,
    @required this.color,
    @required this.size,
    @required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color, //WARNA CARD BERDASARKAN WARNA YANG DITERIMA
      //LALU CHILDNYA KITA BAGI DUA MENGGUNAKAN KOLOM
      child: Row(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width / 4,
            child: Icon(
              icon,
              color: Colors.white,
              size: 80,
            ),
          ),
          Expanded(
              child: Container(
                  child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                total,
                style: TextStyle(
                  fontSize: size.toDouble(),
                  fontWeight: FontWeight.bold,
                ),
              ),
              //DAN YANG KEDUA ADALAH LABEL DATA
              Text(
                label,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              )
            ],
          ))),
        ],
      ),
      elevation: 1,
    );
  }
}
